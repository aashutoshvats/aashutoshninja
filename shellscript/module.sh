#!/bin/bash
function checkLinuxFamily ()  {
        LinuxFamily=`cat /etc/os-release | grep -w ID | tr '"' ' ' | cut -d"=" -f2`
echo "$LinuxFamily"

}

function installNginxOnUbuntu () {
        sudo apt install nginx -y

}

function installNginxOnRhel () {
       sudo yum install nginx -y
}
function removeNginxFromUbuntu () {
        sudo apt remove nginx -y

}

function removeNginxFromRhel () {
       sudo yum remove nginx -y
}
function restartNginxService () {
        sudo systemctl restart nginx
}

