#!/bin/bash
source ./module.sh
source ./var.sh
set -ex 
operation=$1

function installNginx () {
	LinuxFamily=$(checkLinuxFamily)
	if [ $LinuxFamily == "ubuntu" ]
	then 
		installNginxOnUbuntu
	elif [ $LinuxFamily == rhel ]
	then
		
		installNginxOnRhel
	else
		echo "******** Linux Family Must be Ubuntu Or Rhel********* "
	fi
}


function removeNginx () {
        LinuxFamily=$(checkLinuxFamily)
        if [ $LinuxFamily == "ubuntu" ]
        then
                removeNginxFromUbuntu
        elif [ $LinuxFamily == "rhel" ]
        then

                removeNginxFromRhel
        else
                echo "******** Linux Family Must be Ubuntu Or Rhel********* "
        fi
}

function virtualHosting () {
		 doc_root=$1
		 indexHostFile=$2
                 user_name=$3
                 group_name=$4
                 permission=$5
	         nginx_sites_available=/etc/nginx/sites-available
		 conf_file=$6
		 nginxConfTmpl=$7

	 function CreateCustomDocRoot () {
		 if [[ -d "$doc_root" ]]
		then
    			echo "$doc_root Document Root exists on your filesystem."
		else
			sudo mkdir $doc_root
	        	sudo chown $user_name:$group_name $doc_root
		fi
}

	function cpHtmlFile () {
		 sudo cp $indexHostFile $doc_root
		 sudo chown $user_name:$group_name $doc_root/$indexHostFile
		 sudo chmod $permission $doc_root/$indexHostFile

		
	}
	function  setupNginxvHostConf () {
	sudo cp ./$nginxConfTmpl $nginx_sites_available/$conf_file.conf
		
	}
	
	 function symlinkNginxVhost () {
		if [[ -f "/etc/nginx/sites-enabled/$conf_file.conf" ]]
		then
   			 echo "*******************$conf_file.conf This file exists on your filesystem.*******************"
	 	else

			sudo ln -s  $nginx_sites_available/$conf_file.conf /etc/nginx/sites-enabled/$conf_file.conf
		fi
	}	

 CreateCustomDocRoot
  cpHtmlFile
  setupNginxvHostConf
  symlinkNginxVhost
}


case $operation in 
install)
 	installNginx
	 virtualHosting  $doc_root  $indexHostFile $user_name $group_name $permission $conf_file $nginxConfTmpl
	 restartNginxService ;;
 remove)
 	removeNginx ;;
*)
	echo "****************** choose correct option (install OR remove )**************" ;;
esac
