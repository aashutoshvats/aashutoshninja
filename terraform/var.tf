ariable "subnet" {
  type = map(any)
  default = {
    "vpcsubnet"              = "10.0.0.0/16"
    "publicSubnet1"          = "10.0.0.0/25"
    "publicSubnet2"          = "10.0.0.128/25"
    "privateSubnet1"         = "10.0.1.0/25"
    "privateSubnet2"         = "10.0.1.128/25"
    "destination_cidr_block" = "0.0.0.0/0"
    "My_Ip"                  = "103.46.203.195/32"
    "bastion_private_ip"     = "10.0.0.119/32"
  }
}
variable "name" {
  type = map(any)
  default = {
    "vpcName"                    = "ninja-vpc-01"
    "publicSubnet1Name"          = "ninja-pub-sub-01"
    "publicSubnet2Name"          = "ninja-pub-sub-02"
    "privateSubnet1Name"         = "ninja-priv-sub-01"
    "privateSubnet2Name"         = "ninja-priv-sub-02"
    "IGWName"                    = "ninja-igw-01"
    "NATName"                    = "ninja-nat-01"
    "publicSubnetRoutTableName"  = "ninja-route-pub-01/02"
    "privateSubnetRoutTableName" = "ninja-route-priv-01/02"
    "public_Subnet_SG"           = "public_Subnet_SG"
    "private_Subnet_SG"          = "private_Subnet_SG"
    "key_name"                   = "awspemkey"
    "ec2_Instance_pub1"          = "ec2_Instance_pub1"
    "ec2_Instance_pub2"          = "ec2_Instance_pub2"
    "ec2_Instance_pri1"          = "ec2_Instance_pri1"
    "ec2_Instance_pri2"          = "ec2_Instance_pri2"
    "ec2_Instance_pri3"          = "ec2_Instance_pri3"
    "alb"                        = "alb"
  }
}
variable "IME" {
  type = map(any)
  default = {
    "IMEimage" = "ami-0c1a7f89451184c8b"
  }
}
variable "InstanceType" {
  type = map(any)
  default = {
    "instance_type" = "t2.micro"
  }
}
