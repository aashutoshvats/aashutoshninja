


#VPC
resource "aws_vpc" "terraformVpc" {
  cidr_block       = var.subnet.vpcsubnet
  instance_tenancy = "default"
  tags = {
    name = var.name.vpcName
  }
}
# Public Subnet 1
resource "aws_subnet" "publicSubnet1" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.publicSubnet1
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = var.name.publicSubnet1Name
  }
}
# Public Subnet 2
resource "aws_subnet" "publicSubnet2" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.publicSubnet2
  availability_zone       = "ap-south-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = var.name.publicSubnet2Name
  }
}
# Private Subnet 1
resource "aws_subnet" "privateSubnet1" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.privateSubnet1
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = false
  tags = {
    Name = var.name.privateSubnet1Name
  }
}
# Private Subnet 2
resource "aws_subnet" "privateSubnet2" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.privateSubnet2
  availability_zone       = "ap-south-1b"
  map_public_ip_on_launch = false
  tags = {
    Name = "var.name.privateSubnet2Name"
  }
}
#Internet Gateway
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.IGWName
  }
}
# Public Route Table
resource "aws_route_table" "publicRoutTable" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.publicSubnetRoutTableName
  }
}
# Public Route Table Routes
resource "aws_route" "publicRoutes" {
  route_table_id         = aws_route_table.publicRoutTable.id
  destination_cidr_block = var.subnet.destination_cidr_block
  gateway_id             = aws_internet_gateway.IGW.id
}
# Subnet Association to Public Route Table
resource "aws_route_table_association" "publicSubnetAssociation1" {
  subnet_id      = aws_subnet.publicSubnet1.id
  route_table_id = aws_route_table.publicRoutTable.id
}
resource "aws_route_table_association" "publicSubnetAssociation2" {
  subnet_id      = aws_subnet.publicSubnet2.id
  route_table_id = aws_route_table.publicRoutTable.id
}
# Generate Elastic Ip
resource "aws_eip" "NAT_eip" {
  vpc = true
}
# NAT Gateway
resource "aws_nat_gateway" "NAT" {
  allocation_id = aws_eip.NAT_eip.id
  subnet_id     = aws_subnet.publicSubnet1.id
  tags = {
    Name = var.name.NATName
  }
}
# Private Route Table
resource "aws_route_table" "privateRoutTable" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.privateSubnetRoutTableName
  }
}
# Private Route Table Routes
resource "aws_route" "privateRoutes" {
  route_table_id         = aws_route_table.privateRoutTable.id
  destination_cidr_block = var.subnet.destination_cidr_block
  nat_gateway_id         = aws_nat_gateway.NAT.id
}
# Subnet Association to Private Route Table
resource "aws_route_table_association" "privateSubnetAssociation" {
  subnet_id      = aws_subnet.privateSubnet1.id
  route_table_id = aws_route_table.privateRoutTable.id
}
resource "aws_route_table_association" "privateSubnetAssociation2" {
  subnet_id      = aws_subnet.privateSubnet2.id
  route_table_id = aws_route_table.privateRoutTable.id
}
# ************************************************************************************
# Public Security Group
resource "aws_security_group" "publicSubnet" {
  # ... other configuration ...
  vpc_id = aws_vpc.terraformVpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.subnet.My_Ip]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [var.subnet.destination_cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.subnet.destination_cidr_block]
  }
  tags = {
    Name = var.name.public_Subnet_SG
  }
}
#Private SG
resource "aws_security_group" "privateSubnet" {
  vpc_id = aws_vpc.terraformVpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.subnet.bastion_private_ip]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.subnet.destination_cidr_block]
  }
  tags = {
    Name = var.name.private_Subnet_SG
  }
}
# Public Instance 1
resource "aws_instance" "ec2pub1" {
  ami                    = var.IME.IMEimage
  instance_type          = var.InstanceType.instance_type
  key_name               = var.name.key_name
  vpc_security_group_ids = [aws_security_group.publicSubnet.id]
  subnet_id = aws_subnet.publicSubnet1.id
  tags = {
    Name = "var.name.ec2_Instance_pub1"
  }
}
# Public Instance 2
resource "aws_instance" "ec2pub2" {
  ami                    = var.IME.IMEimage
  instance_type          = var.InstanceType.instance_type
  key_name               = var.name.key_name
  vpc_security_group_ids = [aws_security_group.publicSubnet.id]
  subnet_id = aws_subnet.publicSubnet2.id
  tags = {
    Name = "var.name.ec2_Instance_pub2"
  }
}
# Private Instance 1
resource "aws_instance" "ec2pri1" {
  ami                    = var.IME.IMEimage
  instance_type          = var.InstanceType.instance_type
  key_name               = var.name.key_name
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]
  subnet_id = aws_subnet.privateSubnet1.id
  tags = {
    Name = "var.name.ec2_Instance_pri1"
  }
}
# Private Instance 2
resource "aws_instance" "ec2pri2" {
  ami                    = var.IME.IMEimage
  instance_type          = var.InstanceType.instance_type
  key_name               = var.name.key_name
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]
  subnet_id = aws_subnet.privateSubnet2.id
  tags = {
    Name = "var.name.ec2_Instance_pri2"
  }
}
# Private Instance 3
resource "aws_instance" "ec2pri3" {
  ami                    = var.IME.IMEimage
  instance_type          = var.InstanceType.instance_type
  key_name               = var.name.key_name
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]
  subnet_id = aws_subnet.privateSubnet2.id
  tags = {
    Name = "var.name.ec2_Instance_pri3"
  }
}
###############################################################################################################################################
# Application LoadBalancer
resource "aws_lb" "alb" {
  name                       = "application-load-balancer"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.publicSubnet.id]
  subnets                    = [aws_subnet.publicSubnet1.id, aws_subnet.publicSubnet2.id]
  enable_deletion_protection = false
}
resource "aws_lb_target_group_attachment" "nginx_server1" {
  target_group_arn = aws_alb_target_group.nginx.arn
  target_id = aws_instance.ec2pri1.id
  port = 80
}
resource "aws_lb_target_group_attachment" "nginx_server2" {
  target_group_arn = aws_alb_target_group.nginx.arn
  target_id        = aws_instance.ec2pri2.id
  port = 80
}
