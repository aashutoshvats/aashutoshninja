output "terraformVpc" {
  description = "vpc id"
  value       = aws_vpc.terraformVpc.id
}
output "publicSubnet1Id" {
  description = "public Subnet 1 Id"
  value       = aws_subnet.publicSubnet1.id
}
output "publicSubnet2Id" {
  description = "public Subnet 2 Id"
  value       = aws_subnet.publicSubnet2.id
}
output "privateSubnet1Id" {
  description = "private Subnet 1 Id"
  value       = aws_subnet.privateSubnet1.id
}
output "privateSubnet2Id" {
  description = "private Subnet 2 Id"
  value       = aws_subnet.privateSubnet2.id
}
output "IGWId" {
  description = "aws internet gateway  Id "
  value       = aws_internet_gateway.IGW.id
}
output "NATId" {
  description = "aws NAT  gateway  Id "
  value       = aws_nat_gateway.NAT.id
}
output "publicRoutTableId" {
  description = "aws public Route Table Id"
  value       = aws_route_table.publicRoutTable.id
}
output "privateRoutTableId" {
  description = "aws private  Route Table Id"
  value       = aws_route_table.privateRoutTable.id
}
# ******************************************************************************************************
output "aws_security_group_Public_Subnet" {
  description = "aws_security_groupPublicSubnet"
  value       = aws_security_group.publicSubnet.id
}
output "aws_security_group_Private_Subnet" {
  description = "aws_security_groupPrivateSubnet"
  value       = aws_security_group.privateSubnet.id
}
output "aws_public_Instance_1_Id" {
  description = "AWS Instance Public 1  Id"
  value       = aws_instance.ec2pub1.id
}
output "aws_public_Instance_2_Id" {
  description = "AWS Instance Public 2  Id"
  value       = aws_instance.ec2pub2.id
}
output "aws_private_Instance_1_Id" {
  description = "AWS Instance Private 1  Id"
  value       = aws_instance.ec2pri1.id
}
output "aws_private_Instance_2_Id" {
  description = "AWS Instance Private 2  Id"
  value       = aws_instance.ec2pri2.id
}
