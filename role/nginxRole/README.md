Role Name
=========

Nginx Role for install Nginx on servers and configure a virtual hosting

Requirements
------------

No pre Requirement

Role Variables
--------------

 port:  Port number on which nginx listen
 doc_root: Document Root of index.html webpage
 nginx_sites: Path (/etc/nginx/sites-available)
 conf_file: Name of virtual configuration file 
 user_name: User name

Dependencies
------------

No dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

Aashutosh 
